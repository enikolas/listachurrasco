<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!--  Diretiva Page Import -->
<%@ page import="java.util.*"%>
<%@ page import="com.veris.listachurrasco.vo.ItemChurrasco"%>
<%@ page import="com.veris.listachurrasco.enums.TipoEnum" %>
<!--  Diretiva TagLib -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
    <body>
      <jsp:useBean id="itemChurrasco" scope="page" 
          class="com.veris.listachurrasco.vo.ItemChurrasco"/>
      <jsp:useBean id="listaChurrasco" scope="session" 
          class="java.util.ArrayList" 
          type="java.util.List<com.veris.listachurrasco.vo.ItemChurrasco>"/>
      
      <h1>Pesquisar</h1>
      <form id="pesquisar-form" method="get" action="listar.jsp">
      <p>
        <label id="lbl_tipo" for="cmb_tipo">Tipo do Item:</label>
        <select id="cmb_tipo" name="pesqTipoItem">
        	<option value="">Todos</option>
          	<option value="<%= TipoEnum.CARNE.ordinal() %>">Carne</option>
          	<option value="<%= TipoEnum.SALGADO.ordinal() %>">Salgado</option>
          	<option value="<%= TipoEnum.BEBIDA.ordinal() %>">Bebida</option>
         </select>
         <input type="submit" value="Pesquisar" />
      </p>
      </form>
      
      <table>
        <thead>
          <tr>
            <td>Nome</td>
            <td>Quantidade</td>
            <td>Valor</td>
            <td>Tipo do Item</td>
            <td></td>
          </tr>
        </thead>
        <tbody>
          <c:forEach var="item" items="${listaChurrasco}">          
          	<c:if test="${empty param.pesqTipoItem || param.pesqTipoItem == item.tipoItem}">
	          <tr>
	            <td>${item.nome}</td>
	            <td>${item.quantidade}</td>
	            <td>${item.valor}</td>
	            <td>
	            <c:choose>
	              <c:when test="${item.tipoItem == 0}">
	                Carne
	              </c:when>
	              <c:when test="${item.tipoItem == 1}">
	                Salgado
	              </c:when>
	              <c:when test="${item.tipoItem == 2}">
	                Bebida
	              </c:when>
	            </c:choose>
	            </td>
	            <td>
	            	<a href="cadastrar.jsp?id=${item.id}&nome=${item.nome}&quantidade=${item.quantidade}&valor=${item.valor}&tipoItem=${item.tipoItem}">editar</a>
	            	<a href="remover.do?id=${item.id}">remover</a>
	            </td>
	          </tr>
	        </c:if>
          </c:forEach>
        </tbody>        
      </table>
      
      <p>
        <a href="cadastrar.jsp">
          Adicionar Novo Item
        </a>
      </p> 
    </body>
</html>
