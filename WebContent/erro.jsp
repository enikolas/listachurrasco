<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${not empty erros}">
  <p>
    Foi detectado um erro no preenchimento do formulário:      
  </p>      
  <ul>
    <c:forEach var="mensagem_erro" items="${erros}">
      <li>${mensagem_erro}</li>
    </c:forEach>
  </ul>
</c:if>