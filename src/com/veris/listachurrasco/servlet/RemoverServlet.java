package com.veris.listachurrasco.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.veris.listachurrasco.vo.ItemChurrasco;

/**
 * Servlet implementation class RemoverServlet
 */
@WebServlet("/remover.do")
public class RemoverServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public static final String PARAM_ID = "id";
	public static final String SESSION_LISTA_CHURRASCO = "listaChurrasco";

	/** (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String itemId = request.getParameter(PARAM_ID);
		RequestDispatcher rd = request.getRequestDispatcher("/listar.jsp");

		// Se n�o foi passado nenhum id, retorna erro.
		List<String> erros = new ArrayList<String>();
		if (itemId == null || itemId.isEmpty()) {
			erros.add("O ID passado � inv�lido.");
			request.setAttribute("erros", erros);
			rd.forward(request, response);
		}

		// Se o id n�o for um n�mero, retorna um erro.
		int id = 0;
		try {
			id = Integer.valueOf(itemId);
		} catch (Exception e) {
			erros.add("Tipo do item deve ser informado.");
			request.setAttribute("erros", erros);
			rd.forward(request, response);
		}

		System.out.println("itemID: "+itemId + " id: " + id);
		
		List<ItemChurrasco> listaChurrasco = (List<ItemChurrasco>) request
				.getSession().getAttribute(SESSION_LISTA_CHURRASCO);

		/* Procura o id do item atual na lista da sess�o */
		for (int i = 0; i < listaChurrasco.size(); i++) {
			/*
			 * Se o item atual possuir o id que queremos remover, remove ele da
			 * lista e para o loop
			 */
			if (listaChurrasco.get(i).getId() == id) {
				listaChurrasco.remove(i);
				break;
			}
		}
		
		rd.forward(request, response);
	}

}
