<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="com.veris.listachurrasco.enums.TipoEnum" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
    <jsp:include page="erro.jsp" />

    <form id="contato" method="post" action="cadastrar.do">
      <input type="hidden" name="id" value="${param.id}" />
      <p>
        <label id="lbl_nome" for="txt_nome">Nome (Ex.: "Picanha"):</label>
        <br/>
        <input type="text" id="txt_nome" name="nome" value="${param.nome}"/>         
      </p>
      <p>
        <label id="lbl_quantidade" for="txt_quantidade">Quantidade (Ex.: "1 kg")</label>
        <br/>
        <input type="text" id="txt_quantidade" name="quantidade" value="${param.quantidade}"/>
      </p>
      <p>
        <label id="lbl_valor" for="txt_valor">Valor (Ex.: "15.50")</label>
        <br/>
        <input type="text" id="txt_valor" name="valor" value="${param.valor}"/>
      </p>
      <p>
        <label id="lbl_tipo" for="cmb_tipo">Tipo do Item:</label>
        <br/>
        <select id="cmb_tipo" name="tipoItem">
          <c:if test="${param.tipoItem == 0}">
          	<option value="<%= TipoEnum.CARNE.ordinal() %>" selected="selected">Carne</option>
          </c:if>
          <c:if test="${param.tipoItem != 0}">
          	<option value="<%= TipoEnum.CARNE.ordinal() %>">Carne</option>
          </c:if>
          <c:if test="${param.tipoItem == 1}">
          	<option value="<%= TipoEnum.SALGADO.ordinal() %>" selected="selected">Salgado</option>
          </c:if>
          <c:if test="${param.tipoItem != 1}">
          	<option value="<%= TipoEnum.SALGADO.ordinal() %>">Salgado</option>
          </c:if>
          <c:if test="${param.tipoItem == 2}">
          	<option value="<%= TipoEnum.BEBIDA.ordinal() %>" selected="selected">Bebida</option>
          </c:if>
          <c:if test="${param.tipoItem != 2}">
          	<option value="<%= TipoEnum.BEBIDA.ordinal() %>">Bebida</option>
          </c:if>
         </select>
      </p>
      <p>
        <input type="submit" value="Enviar" />
      </p>    
    </form>
</body>
</html>