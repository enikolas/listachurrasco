package com.veris.listachurrasco.vo;

public class ItemChurrasco {
	// id que ir� identificar cada item. Ser� usado para conseguirmos achar qual
	// � o cara que queremos editar ou remover.
	private int id;

	// este � o tipo do item que estamos inserindo na nossa lista de churrasco.
	// algo como: "Carne", "Salgado", "Bebida"... mas os tipos agente pode
	// definir depois.
	private int tipoItem;

	// o nome do item que estamos inserindo. Ex.: "Fardo de Cerveja", "Alcatra",
	// "Contra fil�", ...
	private String nome;

	// a quantidade do item que vamos precisar para o susposto churrasco. o
	// valor � String, para poder ser cadastrado a grandeza do item. Ex.:
	// "1 fardo", "1,5 kg".
	private String quantidade;

	// o valor $$ do item que queremos.
	private double valor;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the tipoItem
	 */
	public int getTipoItem() {
		return tipoItem;
	}

	/**
	 * @param tipoItem
	 *            the tipoItem to set
	 */
	public void setTipoItem(int tipoItem) {
		this.tipoItem = tipoItem;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome
	 *            the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * @return the quantidade
	 */
	public String getQuantidade() {
		return quantidade;
	}

	/**
	 * @param quantidade
	 *            the quantidade to set
	 */
	public void setQuantidade(String quantidade) {
		this.quantidade = quantidade;
	}

	/**
	 * @return the valor
	 */
	public double getValor() {
		return valor;
	}

	/**
	 * @param valor
	 *            the valor to set
	 */
	public void setValor(double valor) {
		this.valor = valor;
	}

}
