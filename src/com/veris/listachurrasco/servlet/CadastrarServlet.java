package com.veris.listachurrasco.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.veris.listachurrasco.enums.TipoEnum;
import com.veris.listachurrasco.vo.ItemChurrasco;

/**
 * Servlet implementation class CadastrarServlet
 */
@WebServlet("/cadastrar.do")
public class CadastrarServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public static final String PARAM_ID = "id";
	public static final String PARAM_NOME = "nome";
	public static final String PARAM_QUANTIDADE = "quantidade";
	public static final String PARAM_VALOR = "valor";
	public static final String PARAM_TIPO = "tipoItem";
	public static final String SESSION_LISTA_CHURRASCO = "listaChurrasco";
	public static final String SESSION_ULTIMO_ID = "ultimoId";

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String itemId = request.getParameter(PARAM_ID);
		String nome = request.getParameter(PARAM_NOME);
		String quantidade = request.getParameter(PARAM_QUANTIDADE);
		String valor = request.getParameter(PARAM_VALOR);
		String tipoItem = request.getParameter(PARAM_TIPO);

		List<String> erros = new ArrayList<String>();
		if (nome == null || nome.isEmpty()) {
			erros.add("O nome do item n�o pode ser vazio.");
		}

		if (quantidade == null || quantidade.isEmpty()) {
			erros.add("A quantidade do item n�o pode ser vazia.");
		}

		if (valor == null || valor.isEmpty()) {
			erros.add("O valor do item n�o pode ser vazio.");
		}

		int tipo = TipoEnum.BEBIDA.ordinal();
		try {
			tipo = Integer.valueOf(tipoItem);
		} catch (Exception e) {
			erros.add("Tipo do item deve ser informado.");
		}

		double val = 0;
		try {
			val = Double.valueOf(valor);
		} catch (Exception e) {
			erros.add("O valor deve ser um n�mero v�lido.");
		}

		if (erros.size() > 0) {
			request.setAttribute("erros", erros);
			RequestDispatcher rd = request
					.getRequestDispatcher("/cadastrar.jsp");
			rd.forward(request, response);
		} else {
			boolean isCadastroNovo = true;

			Integer id;
			// se o cara n�o possuir um ID, � pq ele � um cadastro novo
			if (itemId == null || itemId.isEmpty()) {
				/*
				 * Pega o ultimo ID adicionado na sess�o, servir� para
				 * controlarmos os ids e evitarmos id iguais.
				 */
				id = (Integer) request.getSession().getAttribute(
						SESSION_ULTIMO_ID);

				// se for o primeiro, seta o id como 0;
				if (id == null) {
					id = 0;
				} else {
					// caso contr�rio, adiciona 1 ao id.
					id++;
				}

				// Guarda de volta o id na sess�o para que possamos controlar os
				// ids
				request.getSession().setAttribute(SESSION_ULTIMO_ID, id);
			} else {
				// caso contr�rio � uma edi��o e n�o vamos mecher no id.
				id = Integer.valueOf(itemId);

				// por�m vamos setar a nossa flag como false
				isCadastroNovo = false;
			}

			ItemChurrasco itemChurras = new ItemChurrasco();
			itemChurras.setId(id);
			itemChurras.setNome(nome);
			itemChurras.setTipoItem(tipo);
			itemChurras.setValor(val);
			itemChurras.setQuantidade(quantidade);

			List<ItemChurrasco> listaChurrasco = (List<ItemChurrasco>) request
					.getSession().getAttribute(SESSION_LISTA_CHURRASCO);
			if (listaChurrasco == null) {
				listaChurrasco = new ArrayList<ItemChurrasco>();
				request.getSession().setAttribute(SESSION_LISTA_CHURRASCO,
						listaChurrasco);
			}

			/*
			 * Se n�o for um cadastro novo, vamos primeiro remover o cadastro da
			 * lista da sess�o.
			 */
			if (!isCadastroNovo) {
				/* Procura o id do item atual na lista da sess�o */
				for (int i = 0; i < listaChurrasco.size(); i++) {
					/*
					 * Se o item atual possuir o id que queremos remover, remove
					 * ele da lista e para o loop
					 */
					if (listaChurrasco.get(i).getId() == id) {
						listaChurrasco.remove(i);
						break;
					}
				}
			}

			// Adiciona o cara na lista da sess�o.
			listaChurrasco.add(itemChurras);
			response.sendRedirect("listar.jsp");
		}

	}

}
